# 2FA Audit
The 2FA Audit repository contains scripts to audit 2FA status at Wikimedia wikis. As of now, the notebook needs to be run in the WMF Analytics cluster (to have access to sensitive data such as 2FA enrollment status).
